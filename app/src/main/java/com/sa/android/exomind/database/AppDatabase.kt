package com.sa.android.exomind.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sa.android.exomind.database.dao.AlbumDao
import com.sa.android.exomind.database.dao.PhotoDao
import com.sa.android.exomind.database.dao.UserAlbumDao
import com.sa.android.exomind.database.dao.UserDao

/**
 * The Room database for this app
 */
@Database(
    entities = [
        User::class,
        Photo::class,
        Album::class
    ],
    version = 1, exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun photoDao(): PhotoDao
    abstract fun albumDao(): AlbumDao
    abstract fun userAlbumDao(): UserAlbumDao

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME).build()
        }
    }
}

private const val DATABASE_NAME = "album-db"
