package com.sa.android.exomind.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.sa.android.exomind.database.Photo

@Dao
interface PhotoDao {
    @Insert(onConflict = REPLACE)
    fun insertAll(photos: List<Photo>)

    @Query("SELECT * FROM photos WHERE album_id =:albumId")
    fun findPhotosByAlbumId(albumId: Int): LiveData<List<Photo>>
}