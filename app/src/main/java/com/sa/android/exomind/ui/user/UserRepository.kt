package com.sa.android.exomind.ui.user

import com.sa.android.exomind.database.User
import com.sa.android.exomind.database.dao.UserDao
import com.sa.android.exomind.network.NetworkService

class UserRepository private constructor(
    private val userDao: UserDao,
    private val network: NetworkService
) {
    val users = userDao.getUsers()

    suspend fun getUsers() = network.getUsers()

    suspend fun insertAll(users: List<User>) = userDao.insertAll(users)

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: UserRepository? = null

        fun getInstance(userDao: UserDao, network: NetworkService) =
            instance ?: synchronized(this) {
                instance ?: UserRepository(userDao, network).also { instance = it }
            }
    }
}