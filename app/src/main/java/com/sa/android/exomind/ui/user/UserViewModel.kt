package com.sa.android.exomind.ui.user

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.sa.android.exomind.database.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel(private val userRepository: UserRepository) : ViewModel() {

    val users: LiveData<List<User>> = userRepository.users

    init {
        getUsers()
    }

    private fun getUsers() {
        viewModelScope.launch(Dispatchers.IO) {
            var users = listOf<User>()

            try {
                users = userRepository.getUsers()
            } catch (e: Exception) {
                Log.e(TAG, "Failed to get users", e)
            } finally {
                userRepository.insertAll(users)
            }
        }
    }

    companion object {
        private val TAG: String = UserViewModel::class.java.simpleName
    }
}

/**
 * Factory for creating a [UserViewModel] with a constructor that takes a [UserRepository].
 */
class UserViewModelFactory(
    private val repository: UserRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = UserViewModel(repository) as T
}
