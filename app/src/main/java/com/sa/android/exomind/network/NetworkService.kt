package com.sa.android.exomind.network

import com.sa.android.exomind.database.Album
import com.sa.android.exomind.database.Photo
import com.sa.android.exomind.database.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

class NetworkService {

    val retrofit = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .client(OkHttpClient())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val photoService = retrofit.create(PhotoService::class.java)

    suspend fun getUsers(): List<User> = withContext(Dispatchers.Default) {
        photoService.getUsers()
    }

    suspend fun getAlbums(userId: Int): List<Album> = withContext(Dispatchers.Default) {
        photoService.getAlbums(userId)
    }

    suspend fun getPhotos(albumId: Int): List<Photo> = withContext(Dispatchers.Default) {
        photoService.getPhotos(albumId)
    }
}

interface PhotoService {
    @GET("users")
    suspend fun getUsers(): List<User>

    @GET("users/{userId}/albums")
    suspend fun getAlbums(@Path("userId") userId: Int): List<Album>

    @GET("users/1/photos")
    suspend fun getPhotos(@Query("albumId") albumId: Int): List<Photo>
}