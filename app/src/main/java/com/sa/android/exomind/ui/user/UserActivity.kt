package com.sa.android.exomind.ui.user

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sa.android.exomind.databinding.ActivityUserBinding
import com.sa.android.exomind.utils.Injector

class UserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUserBinding

    private val userViewModel: UserViewModel by viewModels {
        Injector.provideUserFactory(this)
    }

    private val userAdapter = UserAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.userRecycler.apply {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = userAdapter
        }

        userViewModel.users.observe(this, { users ->
            userAdapter.submit(users)

            if (users.isEmpty()) {
                binding.noUser.visibility = View.VISIBLE
                binding.userRecycler.visibility = View.INVISIBLE
            } else {
                binding.noUser.visibility = View.INVISIBLE
                binding.userRecycler.visibility = View.VISIBLE
            }

            binding.progress.visibility = View.INVISIBLE

        })
    }
}