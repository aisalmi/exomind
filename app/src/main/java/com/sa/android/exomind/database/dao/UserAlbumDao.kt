package com.sa.android.exomind.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.sa.android.exomind.database.UserAlbum

@Dao
interface UserAlbumDao {
    @Transaction
    @Query("SELECT * FROM users WHERE id = :userId")
    fun findAlbumsByUserId(userId: Int): LiveData<UserAlbum>
}