package com.sa.android.exomind.ui.user

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.sa.android.exomind.R
import com.sa.android.exomind.database.User
import com.sa.android.exomind.ui.album.AlbumActivity

class UserAdapter : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    private val differ = AsyncListDiffer(this, object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: User, newItem: User) = oldItem.id == newItem.id
    })

    class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        ) {
        val name: TextView = itemView.findViewById(R.id.name)
        val email: TextView = itemView.findViewById(R.id.email)
        val address: TextView = itemView.findViewById(R.id.address)
        val phone: TextView = itemView.findViewById(R.id.phone)
        val website: TextView = itemView.findViewById(R.id.website)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = differ.currentList[position]

        holder.name.text = user.name
        holder.email.text = user.email
        holder.address.text = user.address.toString()
        holder.phone.text = user.phone
        holder.website.text = user.website
        holder.itemView.setOnClickListener {
            val context = holder.itemView.context
            val intent = Intent(context, AlbumActivity::class.java).apply {
                putExtra(AlbumActivity.EXTRA_USER_ID, user.id)
            }
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun submit(users: List<User>) {
        differ.submitList(users)
    }
}