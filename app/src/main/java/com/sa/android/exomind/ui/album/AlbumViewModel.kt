package com.sa.android.exomind.ui.album

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.sa.android.exomind.database.Album
import com.sa.android.exomind.database.UserAlbum
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AlbumViewModel(private val repository: AlbumRepository, private val userId: Int) : ViewModel() {

    var userAlbums: LiveData<UserAlbum> = repository.findAlbumsByUserId(userId)

    init {
        getAlbums()
    }

    private fun getAlbums() {
        viewModelScope.launch(Dispatchers.IO) {
            var albums = listOf<Album>()

            try {
                albums = repository.getAlbums(userId)
            } catch (e: Exception) {
                Log.e(TAG, "Failed to get albums", e)
            } finally {
                repository.insertAll(albums)
                //userAlbums = repository.findAlbumsByUserId(userId)
            }
        }
    }

    companion object {
        private val TAG: String = AlbumViewModel::class.java.simpleName
    }
}

/**
 * Factory for creating a [AlbumViewModel] with a constructor that takes a [AlbumRepository] and a [userId] .
 */
class AlbumViewModelFactory(
    private val repository: AlbumRepository,
    private val userId: Int
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = AlbumViewModel(repository, userId) as T
}
