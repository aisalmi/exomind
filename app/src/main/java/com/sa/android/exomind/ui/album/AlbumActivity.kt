package com.sa.android.exomind.ui.album

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sa.android.exomind.databinding.ActivityAlbumBinding
import com.sa.android.exomind.utils.Injector

class AlbumActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAlbumBinding

    private val albumViewModel: AlbumViewModel by viewModels {
        Injector.provideAlbumFactory(this, intent.extras!!.getInt(EXTRA_USER_ID))
    }

    private val albumAdapter = AlbumAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAlbumBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.albumRecycler.apply {
            layoutManager = GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)
            adapter = albumAdapter
        }

        albumViewModel.userAlbums.observe(this, { userAlbums ->
            albumAdapter.submit(userAlbums.albums)
            binding.progress.visibility = View.INVISIBLE
            binding.albumRecycler.visibility = View.VISIBLE
        })
    }

    companion object {
        const val EXTRA_USER_ID = "EXTRA_USER_ID"
        private val TAG = AlbumActivity::class.java.simpleName
    }
}