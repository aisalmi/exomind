package com.sa.android.exomind.ui.photo

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.sa.android.exomind.database.Photo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PhotoViewModel(private val repository: PhotoRepository, private val albumId: Int) : ViewModel() {

    var photos = repository.findPhotosByAlbumId(albumId)

    init {
        getPhotos()
    }

    private fun getPhotos() {
        viewModelScope.launch(Dispatchers.IO) {
            var photos = listOf<Photo>()

            try {
                photos = repository.getPhotos(albumId)
            } catch (e: Exception) {
                Log.e(TAG, "Failed to get photos for $albumId", e)
            } finally {
                Log.e(TAG, "FOUND ${photos.size} photos")
                repository.insertAll(photos)
                //userAlbums = repository.findAlbumsByUserId(userId)
            }
        }
    }

    companion object {
        private val TAG: String = PhotoViewModel::class.java.simpleName
    }
}

/**
 * Factory for creating a [PhotoViewModel] with a constructor that takes a [PhotoRepository] and a [albumId].
 */
class PhotoViewModelFactory(
    private val repository: PhotoRepository,
    private val albumId: Int
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = PhotoViewModel(repository, albumId) as T
}
