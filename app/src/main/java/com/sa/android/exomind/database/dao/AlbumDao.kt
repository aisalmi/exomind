package com.sa.android.exomind.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import com.sa.android.exomind.database.Album

@Dao
interface AlbumDao {

    @Insert(onConflict = REPLACE)
    suspend fun insertAll(albums: List<Album>)
}