package com.sa.android.exomind.ui.photo

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sa.android.exomind.databinding.ActivityPhotoBinding
import com.sa.android.exomind.utils.Injector

class PhotoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPhotoBinding

    private val photoViewModel: PhotoViewModel by viewModels {
        Injector.providePhotoFactory(this, intent.extras!!.getInt(EXTRA_ALBUM_ID))
    }

    private val photoAdapter = PhotoAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPhotoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.photoRecycler.apply {
            layoutManager = GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)
            adapter = photoAdapter
        }

        photoViewModel.photos.observe(this, { photos ->
            photoAdapter.submit(photos)
            binding.progress.visibility = View.INVISIBLE
            binding.photoRecycler.visibility = View.VISIBLE
        })
    }

    companion object {
        const val EXTRA_ALBUM_ID = "EXTRA_ALBUM_ID"
    }

}