package com.sa.android.exomind.ui.album

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.sa.android.exomind.R
import com.sa.android.exomind.database.Album
import com.sa.android.exomind.ui.photo.PhotoActivity

class AlbumAdapter : RecyclerView.Adapter<AlbumAdapter.ViewHolder>() {

    private val differ = AsyncListDiffer(this, object : DiffUtil.ItemCallback<Album>() {
        override fun areItemsTheSame(oldItem: Album, newItem: Album) = oldItem.albumId == newItem.albumId
        override fun areContentsTheSame(oldItem: Album, newItem: Album) = oldItem.albumId == newItem.albumId
    })

    class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_album, parent, false)
        ) {
        val title: TextView = itemView.findViewById(R.id.title)
        val photos: TextView = itemView.findViewById(R.id.photos)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val album = differ.currentList[position]

        holder.title.text = album.title

        holder.itemView.setOnClickListener {
            val context = holder.itemView.context
            val intent = Intent(context, PhotoActivity::class.java).apply {
                putExtra(PhotoActivity.EXTRA_ALBUM_ID, album.albumId)
            }
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun submit(albums: List<Album>) {
        differ.submitList(albums)
    }
}