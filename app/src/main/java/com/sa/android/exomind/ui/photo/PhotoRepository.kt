package com.sa.android.exomind.ui.photo

import com.sa.android.exomind.database.Photo
import com.sa.android.exomind.database.dao.PhotoDao
import com.sa.android.exomind.network.NetworkService

class PhotoRepository private constructor(
    private val photoDao: PhotoDao,
    private val network: NetworkService
) {
    fun findPhotosByAlbumId(albumId: Int) = photoDao.findPhotosByAlbumId(albumId)

    suspend fun getPhotos(albumId: Int) = network.getPhotos(albumId)

    suspend fun insertAll(photos: List<Photo>) = photoDao.insertAll(photos)

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: PhotoRepository? = null

        fun getInstance(photoDao: PhotoDao, network: NetworkService) =
            instance ?: synchronized(this) {
                instance ?: PhotoRepository(photoDao, network).also { instance = it }
            }
    }
}