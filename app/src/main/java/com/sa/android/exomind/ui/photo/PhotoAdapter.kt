package com.sa.android.exomind.ui.photo

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.sa.android.exomind.BuildConfig
import com.sa.android.exomind.R
import com.sa.android.exomind.database.Photo

class PhotoAdapter : RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {

    private val differ = AsyncListDiffer(this, object : DiffUtil.ItemCallback<Photo>() {
        override fun areItemsTheSame(oldItem: Photo, newItem: Photo) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Photo, newItem: Photo) = oldItem.id == newItem.id
    })

    class ViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        ) {
        val title: TextView = itemView.findViewById(R.id.title)
        val thumbnail: ImageView = itemView.findViewById(R.id.thumbnail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo = differ.currentList[position]

        holder.title.text = photo.title

        val url = GlideUrl(
            photo.url.trim(),
            LazyHeaders.Builder().addHeader("User-Agent", USER_AGENT).build()
        )

        Glide.with(holder.itemView)
            .load(url)
            .fitCenter()
            .error(R.drawable.ic_bg_sadcloud)
            .into(holder.thumbnail)
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun submit(photos: List<Photo>) = differ.submitList(photos)

    companion object {
        const val USER_AGENT = "Photos/${BuildConfig.VERSION_CODE} (Android)"
    }
}