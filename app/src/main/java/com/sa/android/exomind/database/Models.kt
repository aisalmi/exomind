package com.sa.android.exomind.database

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "users")
data class User(
    @PrimaryKey val id: Int,
    @Embedded val address: Address,
    @Embedded val company: Company,
    val email: String,
    val name: String,
    val phone: String,
    val username: String,
    val website: String
)

data class Address(
    val city: String,
    @Embedded val geo: Geo,
    val street: String,
    val suite: String,
    @SerializedName("zipcode") @ColumnInfo(name = "zipcode") val zipCode: String?
) {
    override fun toString(): String {
        return "$suite $street $city $zipCode"
    }
}

data class Geo(
    val lat: String,
    val lng: String
)

data class Company(
    val bs: String,
    @ColumnInfo(name = "catch_phrase") val catchPhrase: String,
    @ColumnInfo(name = "company_name") val name: String
)

@Entity(tableName = "albums")
data class Album(
    @SerializedName("id") @ColumnInfo(name = "album_id") @PrimaryKey var albumId: Int,
    @ColumnInfo(name = "user_id") var userId: String,
    var title: String
)

@Entity(tableName = "photos")
data class Photo(
    @ColumnInfo(name = "album_id") var albumId: String,
    @PrimaryKey var id: Long,
    var title: String,
    var url: String,
    @ColumnInfo(name = "thumbnail_url") var thumbnailUrl: String
)

data class UserAlbum(
    @Embedded val user: User,
    @Relation(parentColumn = "id", entityColumn = "user_id") val albums: List<Album>
)