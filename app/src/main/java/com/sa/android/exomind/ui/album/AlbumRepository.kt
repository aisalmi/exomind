package com.sa.android.exomind.ui.album

import com.sa.android.exomind.database.Album
import com.sa.android.exomind.database.dao.AlbumDao
import com.sa.android.exomind.database.dao.UserAlbumDao
import com.sa.android.exomind.network.NetworkService

class AlbumRepository private constructor(
    private val userAlbumDao: UserAlbumDao,
    private val albumDao: AlbumDao,
    private val network: NetworkService
) {
    fun findAlbumsByUserId(userId: Int) = userAlbumDao.findAlbumsByUserId(userId)

    suspend fun getAlbums(userId: Int) = network.getAlbums(userId)

    suspend fun insertAll(albums: List<Album>) = albumDao.insertAll(albums)

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: AlbumRepository? = null

        fun getInstance(userAlbumDao: UserAlbumDao, albumDao: AlbumDao, network: NetworkService) =
            instance ?: synchronized(this) {
                instance ?: AlbumRepository(userAlbumDao, albumDao, network).also { instance = it }
            }
    }
}