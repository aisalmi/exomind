package com.sa.android.exomind.utils

import android.content.Context
import com.sa.android.exomind.database.AppDatabase
import com.sa.android.exomind.network.NetworkService
import com.sa.android.exomind.ui.album.AlbumRepository
import com.sa.android.exomind.ui.album.AlbumViewModelFactory
import com.sa.android.exomind.ui.photo.PhotoRepository
import com.sa.android.exomind.ui.photo.PhotoViewModelFactory
import com.sa.android.exomind.ui.user.UserRepository
import com.sa.android.exomind.ui.user.UserViewModelFactory

interface ViewModelFactoryProvider {
    fun provideUserFactory(context: Context): UserViewModelFactory
    fun provideAlbumFactory(context: Context, userId: Int): AlbumViewModelFactory
    fun providePhotoFactory(context: Context, albumId: Int): PhotoViewModelFactory
}

val Injector: ViewModelFactoryProvider
    get() = currentInjector

@Volatile
private var currentInjector: ViewModelFactoryProvider = DefaultViewModelProvider

private object DefaultViewModelProvider : ViewModelFactoryProvider {
    private fun network() = NetworkService()

    private fun userDao(context: Context) = AppDatabase.getInstance(context.applicationContext).userDao()
    private fun userAlbumDao(context: Context) = AppDatabase.getInstance(context.applicationContext).userAlbumDao()
    private fun albumDao(context: Context) = AppDatabase.getInstance(context.applicationContext).albumDao()
    private fun photoDao(context: Context) = AppDatabase.getInstance(context.applicationContext).photoDao()

    private fun getUserRepository(context: Context): UserRepository {
        return UserRepository.getInstance(
            userDao(context),
            network()
        )
    }

    private fun getAlbumRepository(context: Context): AlbumRepository {
        return AlbumRepository.getInstance(
            userAlbumDao(context),
            albumDao(context),
            network()
        )
    }

    private fun getPhotoRepository(context: Context): PhotoRepository {
        return PhotoRepository.getInstance(
            photoDao(context),
            network()
        )
    }

    override fun provideUserFactory(context: Context): UserViewModelFactory {
        return UserViewModelFactory(getUserRepository(context))
    }

    override fun provideAlbumFactory(context: Context, userId: Int): AlbumViewModelFactory {
        return AlbumViewModelFactory(getAlbumRepository(context), userId)
    }

    override fun providePhotoFactory(context: Context, albumId: Int): PhotoViewModelFactory {
        return PhotoViewModelFactory(getPhotoRepository(context), albumId)
    }
}
